import { isObservable, Observable } from 'rxjs';
import { ValidationError } from './models';

export function isPromise(arg: unknown): boolean {
  return arg instanceof Promise;
}

export function toPromise<T>(arg: T | Promise<T> | Observable<T>): Promise<T> {
  let result: Promise<T>;
  if (isObservable(arg)) {
    result = arg.toPromise();
  } else if (isPromise(arg)) {
    result = arg as Promise<T>;
  } else {
    result = Promise.resolve(arg);
  }
  return result;
}

export function convertToFormError(errors: ValidationError[] = []): { [key: string]: unknown } {
  const result = {};
  errors.forEach((error) => {
    result[error.field] = error.errorCode.reduce((data, value) => {
      data[value] = true;
      return data;
    }, {});
  });
  return result;
}
